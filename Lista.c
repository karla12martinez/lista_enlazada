#include <stdio.h>
#include <stdlib.h>

typedef struct node{	//Estructura del nodo
	int dato;
	struct node *sgte;
}*nodo,tipoNodo;

typedef struct{		//Estructura de la lista
	nodo front;
	nodo last;
}*lista, tipoLista;

nodo crearNodo(int valor){		//Metodo para crear un Nodo con un valor
	nodo elemento=(nodo) malloc(sizeof(tipoNodo));
	elemento->dato= valor;
	elemento->sgte = NULL;
	return elemento;
}
lista crearLista(){		//Metodo para crear la lista
  lista List = (lista) malloc(sizeof(tipoLista));
  List->front = NULL;
  List->last = NULL;
  return List;
}
void insertFront(int valor, lista List){	//Metodo para agregar al inicio de la fila, tiene como parametro un valor entero y la lista a la que se va a guardar
	nodo elemento=crearNodo(valor);
	if(empty(List)){	//Metodo para saber si esta vacia, se puede encontrar declarado mas abajo
		elemento->sgte=NULL;
		List->front=elemento;
		List->last=elemento;
	}
	else{
	    elemento->sgte = List->front;
	    List->front=elemento;
    }
}
void insertLast(int valor, lista List){	//Metodo para agragar al final de la fila, tiene los mismos parametros que insertFront
	if(List->last==NULL){
	nodo elemento=crearNodo(valor);
	List->front=elemento;
	List->last=elemento;
}else{
	nodo elemento=crearNodo(valor);
	elemento->sgte=NULL;
	List->last->sgte=elemento;
	List->last=elemento;
}
}
int first(lista List){	//Regresa el primer dato sin eliminar
	int primero;
	if(List->front==NULL){
		printf("\nLa lista esta vacia");
	}
	primero=List->front->dato;
	return primero;
}
int last(lista List){	//Regresa el ultimo dato sin eliminarlo
	int ultimo;
	ultimo=List->last->dato;
	return ultimo;
}

int empty(lista List){		//Metodo nombrado anteriormente para saber si esta vacia
	return(List->front==NULL);
}

void mostrarLista(lista List){		//Metodo para mostrar todos los datos de la lista
	nodo aux;
	aux=List->front;
	if(aux==NULL){
		printf("\nLa lista esta vacia");
	}
	while(aux!=NULL){
		printf("->");
		printf("%d",aux->dato);
		aux=aux->sgte;
	}
}
int main(){
	int x=0,i,dato=0,pos=0;
	lista d=crearLista();
	do{
		printf("\n--------------------------------------------------\n");
		printf("1. Para agregar al principio de la fila\n");
		printf("2. Para agregar al final de la fila\n");
		printf("3. Para mostrar todos los datos \n");
		printf("0. Para salir\n");
		printf("--------------------------------------------------\n");
		printf("Digite una opcion: ");
		scanf("%d",&x);
		switch(x){
			case 0:
				printf("\nHa salido con exito");
			break;
			case 1:
				printf("\nDigite el dato a ingresar: ");
				scanf("%d",&dato);
				insertFront(dato,d);
			break;
			case 2:
				printf("\nDigite el dato a ingresar: ");
				scanf("%d",&dato);
				insertLast(dato,d);
			break;
		
			case 3:
				mostrarLista(d);
			break;
			default:
				printf("\nDigite una opcion correcta");
		}
	}while(x!=0);
	return 0;
}
